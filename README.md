# SuperRetina for Retinal Image Matching
This is one of the repositories of source code the my project "Improved Retinal Fundus Image Registration through Illumination Matching"


## Report:
The PDF of the report can be found [here](./Improving_RetinalFundusImage_Registration_through_IlluminationMatching.pdf)

## Environment
We used Anaconda to setup a deep learning workspace that supports PyTorch. Run the following script to install all the required packages.

``` 
git clone https://gitlab.com/srag21/SuperRetina-illumination.git
cd SuperRetina-illumination
conda env create -f SuperRetina_illumination.yml
```

## Downloads

### Data and Models
Download both data and models from this link: https://drive.google.com/file/d/1e15HeljUbTOqxcSQoWY_jRa_IRMiuRVo/view?usp=sharing and extract it project home directory. 



### Fine-tuning on SIGF 

Fine-tune SuperRetina on SIGF by updating (config/finetune_on_SIGF.yaml) and running
```
python finetune_on_SIGF_768.py
```

### Reproducing Results in the Report: 

| Model                  | Dataset       | Failure Rate | Inaccurate Rate | Acceptable Rate |
|------------------------|---------------|--------------|-----------------|-----------------|
| SuperRetina - original | FIRE Original | 0.00         | 1.50            | 98.50           |
| SuperRetina - original | SIGF original | 5.71         | 17.14            | 77.14           |

```
python test_on_SIGF.py
```
| Model                  | Dataset       | Failure Rate | Inaccurate Rate | Acceptable Rate |
|------------------------|---------------|--------------|-----------------|-----------------|
| SuperRetina - original | SIGF original | 5.71         | 17.14           | 77.14           |
| SuperRetina - original | SIGF enhanced | 4.29         | 17.14           | 78.57           |
| SuperRetina fine-tuned | SIGF original | 4.29         | 8.57            | 87.14           |
| SuperRetina fine-tuned | SIGF enhanced | 2.86         | 8.57            | 88.57           |
```
python test_on_SIGF_original_model_original_data.py
python test_on_SIGF_original_model_enhanced_data.py
python test_on_SIGF_finetuned_model_enhanced_data.py
python test_on_SIGF_finetuned_model_enhanced_data.py
```

## Contact
If you encounter any issue when running the code, please email

+ Shruti Raghavan (shrutiraghavan@utexas.edu)

